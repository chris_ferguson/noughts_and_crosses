<?php
	class Game {

		public $result = null;

		function __construct ($game_data) {
			$this->game_data = $game_data;
		}

		public function calculateResult () {

			$game = $this->game_data;
			$winner = array();

			//check rows for winner
			foreach ($game as $row) {
				if ($row[0] == $row[1] && $row[1] == $row[2]) {
					array_push($winner, $row[0]);
				}
			}
			//check cols for winner
			for ($x=0; $x<3; $x++) {
				if ($game[0][$x] == $game[1][$x] && $game[1][$x] == $game[2][$x]) {
					array_push($winner, $game[0][$x]);
				}
			}
			//check diagonals for winner
			if ($game[0][0] == $game[1][1] && $game[1][1] == $game[2][2]) {
				array_push($winner, $game[1][1]);
			}
			if ($game[0][2] == $game[1][1] && $game[1][1] == $game[2][0]) {
				array_push($winner, $game[1][1]);
			}

			//push winner info - check not more than one winner
			if (count($winner)<1) {
				$this->result = 'Draws';
				return 'Draws';
			} elseif (count($winner)>1) {
				exit ('ERROR: Game number '.($key + 1).' has more than one winning sequence. Exiting.');
			} else {
				$this->result = $winner[0].' Wins';
				return $winner[0].' Wins';
			}
		}

		public function pushToDB ($conn) {
			$game_data = json_encode($this->game_data);
			$upload_query = "INSERT INTO noughts_crosses.games (result, game_json) VALUES ('$this->result','$game_data')";
			try {
				$stmt = $conn->prepare($upload_query);
				$stmt->execute();
			} catch(PDOException $e) {
				echo 'ERROR: ' . $e->getMessage();
			}
		}

	}
?>
