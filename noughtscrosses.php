<?php
	require_once('class_NoughtsCrosses.php');
	$class = new NoughtsCrosses;

	if ($argv[1] == 'results') {
		echo $class->get_aggregate_results();
	} elseif ($argv[1] == 'calculate') {
		echo $class->calculate_winners(STDIN);
	} else {
		echo "Usage: noughtscrosses.php [ACTIONS]
		Actions:
			results - Output alltime results from all games ever.
			calculate - Calculate results from round of games provided via STDIN.";
	}
?>
