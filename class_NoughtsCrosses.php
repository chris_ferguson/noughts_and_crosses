<?php
	class NoughtsCrosses {

		//GET AGGREGATE RESULTS FUNCTION
		public function get_aggregate_results() {

			$results = array('X Wins'=>0, 'O Wins'=>0, 'Draws'=>0);
			$result_txt = '';

			try {
				$conn = new PDO('mysql:host=localhost;dbname=noughts_crosses', 'rw_user', 'strong_password');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch(PDOException $e) {
				exit ('ERROR: ' . $e->getMessage());
			}

			$results_qry = "SELECT result, COUNT(*) as res_cnt FROM games GROUP BY result";
			try {
				$stmt = $conn->prepare($results_qry);
				$stmt->execute();

				foreach ($stmt as $row) {
					$results[$row['result']] = $row['res_cnt'];
				}

				$result_txt = 	"AGGREGATE RESULTS\n".
								"------------------\n";

				foreach ($results as $name=>$result) {
					$result_txt.= $name.": ".$result."\n";
				}

				return $result_txt;

			} catch(PDOException $e) {
				exit ('ERROR: ' . $e->getMessage());
			}
		}


		//CALCULATE WINNERS FUNCTION
		public function calculate_winners ($stdin) {

			$games = $this->parseGameInput($stdin);
			$results = $this->calculateResults($games);
			$this->pushResults($games);

			//return results
			$result_txt = '';
			foreach ($results as $name=>$result) {
				$result_txt.= $name.": ".$result."\n";
			}
			return $result_txt;

		}

		//PARSE GAME INFO FROM STDINPUT
		private function parseGameInput($stdin) {

			require_once('class_Game.php');

			$game_data = array();
			$games = array();
			$game_line_cnt = 0; //indicates line number in the game
			$new_line_cnt = 0; //var for checking three new lines between games
			$line_no = 0;

			//get data from stdinput and create games array
			try {
				while ($line = fgets($stdin)) {

					$line_no++;

					if ($line==PHP_EOL) {
						$new_line_cnt++;
						$game_line_cnt = 0;
					} else {
						$game_line_cnt++;

						//check for 3 eol chars - exit on fail
						if ($new_line_cnt!==3 && $line_no>3 && $game_line_cnt==1) {
							exit ("ERROR Line ".$line_no." - Exiting: Expected 3 End Of Line characters between games and found ".$new_line_cnt);
						} else {
							$new_line_cnt = 0;
						}

						//check no of lines in game - exit on fail
						if ($game_line_cnt>3) {
							exit ("ERROR Line ".$line_no." - Exiting: Expected 3 lines in Game and found ".$game_line_cnt);
						}

						//error checking line - exit on fail
						switch ($line) {
							case strlen(rtrim($line))!==3:
								exit ("ERROR (".$line_no."): String not made up of 3 Characters (".rtrim($line).")");
							case (substr_count($line,'X') + substr_count($line,'O')) !== strlen(rtrim($line)):
								exit ("ERROR (".$line_no."): String not made up of 'X' and 'O' Characters - case sensitive (".rtrim($line).")");
						}

						//add to game array
						$game_data[$game_line_cnt - 1] = str_split(rtrim($line));

						//if game line = 3 create class and add to overall array
						if ($game_line_cnt==3) {
							$game = New Game ($game_data);
							array_push($games, $game);
							$game_line_cnt = 0;
						}
					}
				}
			} catch (Exception $e) {
				exit ('ERROR: Could not obtain standard input: '.$e->getMessage());
			}

			return $games;
		}

		//CALCULATE RESULTS FUNCTION
		private function calculateResults($games){

			$results = array('X Wins'=>0, 'O Wins'=>0, 'Draws'=>0);

			if (count($games)) {
				foreach ($games as $key=>$game) {
					$winner = $game->calculateResult();
					//push winner info - check not more than one winner
					$results[$winner]++;
				}

				return $results;

			} else {
				exit ("No games found in Input.");
			}
		}

		//PUSH RESULTS TO MYSQL
		private function pushResults($games) {
			try {
				$conn = new PDO('mysql:host=localhost;dbname=noughts_crosses', 'rw_user', 'strong_password');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch(PDOException $e) {
				exit ('ERROR: ' . $e->getMessage());
			}
			//create table query
			$create_tbl_qry = "CREATE TABLE IF NOT EXISTS noughts_crosses.games
								(
									id INT NOT NULL AUTO_INCREMENT,
									result VARCHAR(6) NULL,
									game_json JSON NULL,
									item_added DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
									PRIMARY KEY (id),
									INDEX (result(6))
								)
			 					ENGINE = InnoDB;";
			$stmt = $conn->prepare($create_tbl_qry);
			$stmt->execute();
			//upload games
			foreach ($games as $game) {
				$game->pushToDB($conn);
			}
			$conn = null;
		}
	}
?>
